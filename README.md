## About

Een kleine scraper gemaakt met scrapy voor het scrapen van appartementen op Funda en Pararius.  
Op de huurmarkt in Rotterdam gaat het erg snel. Daarom wilde ik er graag snel bij zijn indien er een appartement op de markt kwam.

De scraper checkte elke 5 minuten op Funda en Pararius of er appartementen beschikbaar waren die voldeden aan mijn gestelde eisen.  
Als er er een nieuw appartement werd gevonden, werd deze in de database gezet en een melding via discord webhook op een private server geplaatst.

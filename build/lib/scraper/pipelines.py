# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from sqlalchemy.orm import sessionmaker
from scrapy.exceptions import DropItem, CloseSpider
from scraper.models import Listing, db_connect, create_table
from scrapy.utils.project import get_project_settings
from dhooks import Webhook, Embed
class ScraperPipeline:
    def process_item(self, item, spider):
        return item

class ListingPipeline:
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self,item,spider):
        session = self.Session()
        listing = Listing()
        listing.straat = item['straat']
        listing.postcode = item['postcode']
        listing.prijs = item['prijs']
        listing.plaats = item['plaats']
        listing.oppervlakte = item['oppervlakte']
        listing.kamers = item['kamers']
        listing.listing_id = item['listing_id']
        listing.listing_url = item['listing_url']
        listing.site_id = item['site_id']
        
        try:
            session.add(listing)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
        
        return item

class DuplicatesPipeline(object):
    def __init__(self):

        engine=db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self,item,spider):
        session = self.Session()
        exist_listing = session.query(Listing).filter_by(listing_id = item['listing_id']).first()
        if exist_listing is not None:
            
            raise DropItem("Duplicate found: %s" % item['listing_id'])
            session.close()
        else:
           
            return item
            session.close()
   
class DiscordPipeline:
    def process_item(self,item,spider):
        hook = Webhook(get_project_settings().get('DISCORD_WEBHOOK'))
        embed = Embed(title=item['straat'], url=item['listing_url'])
        embed.add_field(name = 'oppervlakte', value=str(item['oppervlakte']) + 'm2')
        embed.add_field(name = 'kamers', value=item['kamers'])
        embed.add_field(name = 'prijs', value= "€" + str(item['prijs']))
        embed.add_field(name = 'plaats', value=item['plaats'])
        hook.send(embed = embed)
        return item
    
            

  
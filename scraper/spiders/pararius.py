import scrapy
from scrapy.loader import ItemLoader
from scraper.items import Listing

class ParariusSpider(scrapy.Spider):
    name = 'pararius'
    domain='https://www.pararius.nl'
    allowed_domains = ['pararius.nl']
    site_id = 2
    max_pages = 5
    page_count = 3
    start_urls = ['http://pararius.nl/huurwoningen/rotterdam']

    def parse(self, response):
        search_result = response.css('.search-list__item--listing')
       

        for listing in search_result:
            adres = self.parse_adres(listing)
            postcode = None if adres is None else adres['postcode']
            plaats = None if adres is None  else adres['plaats']
            kenmerken = self.parse_kenmerken(listing)    

            loader = ItemLoader(item=Listing(), selector=listing, )
            loader.context['domain'] = self.domain
            loader.add_css('straat', '.listing-search-item__link::text')
            loader.add_value('postcode', postcode)
            loader.add_value('plaats', plaats)
            loader.add_css('prijs', '.listing-search-item__price::text')
            loader.add_value('oppervlakte', kenmerken['oppervlakte'])
            loader.add_value('kamers', kenmerken['kamers'])
            loader.add_value('listing_id', self.parse_id(listing))
            loader.add_css('listing_url', '.listing-search-item__link::attr(href)')
            loader.add_value('site_id', self.site_id)
            yield loader.load_item()

        next_page = response.css('.pagination__link--next::attr(href)').get()
      
        if next_page is not None and self.page_count < self.max_pages:
            self.page_count = self.page_count + 1
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_adres(self,listing):
            adres=listing.css('.listing-search-item__location::text').extract()
            filteredAdres = [a.strip() for a in adres if a.strip() ][0].split(" ")
            if adres is not None:
              
                if len(filteredAdres) == 4:
                    return {
                        'postcode': filteredAdres[0] + filteredAdres[1],
                        'plaats': filteredAdres[2]
                    }
                else:
                    return {
                        'postcode': filteredAdres[0] + filteredAdres[1],
                        'plaats': filteredAdres[2] + " " + filteredAdres[3]
                    }
            
        
            return None
    def parse_kenmerken(self,listing):
            kenmerken = listing.css('.illustrated-features__description::text').getall()
            filteredKenmerken = [k.strip() for k in kenmerken if k.strip() ]
            parsedKenmerken = kenmerken[0].split(" ")[0]
            return {
                'oppervlakte': filteredKenmerken[0],
                'kamers': filteredKenmerken[1],
               
            }

    def parse_id (self, listing):
        listing_url = listing.css('.listing-search-item__link::attr(href)').get()
        listing_id = listing_url.split('/')[3]
        return listing_id

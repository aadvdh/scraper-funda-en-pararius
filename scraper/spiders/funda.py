import scrapy
from scrapy.loader import ItemLoader
from scraper.items import Listing

class FundaSpider(scrapy.Spider):
    name = 'funda'
    domain='https://www.funda.nl'
    allowed_domains = ['funda.nl']
    site_id = 1
    max_pages = 5
    page_count = 3
    start_urls = ['https://funda.nl/huur/rotterdam/sorteer-datum-af/p1']

    def parse(self, response):
        search_result = response.css('.search-result-main')
        for listing in search_result:
            loader = ItemLoader(item=Listing(), selector=listing, )

            adres = self.parse_adres(listing)
            postcode = None if adres is None else adres['postcode']
            plaats = None if adres is None  else adres['plaats']
            loader.context['domain'] = self.domain
            loader.add_css('straat', '.search-result__header-title::text')
            loader.add_value('postcode', postcode)
            loader.add_value('plaats', plaats)
            loader.add_value('prijs', self.parse_prijs(listing))
            loader.add_value('oppervlakte', self.parse_oppervlakte(listing))
            loader.add_value('kamers', self.parse_kamers(listing))
            loader.add_css('listing_id', 'a::attr(data-search-result-item-anchor)')
            loader.add_css('listing_url', '.search-result-media a::attr(href)')
            loader.add_value('site_id', self.site_id)
            yield loader.load_item()

        next_page = response.css('a[rel="next"]::attr(href)').get()
       
        if next_page is not None and self.page_count < self.max_pages:
            self.page_count = self.page_count + 1
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
        pass
    def parse_adres(self,listing):
        adres=listing.css('.search-result__header-subtitle::text').get().split(' ')
        filteredAdres = [a.strip() for a in adres if a.strip() ]
       
        if adres is not None:
            if len(filteredAdres) == 1:
                return {
                    'postcode': "None",
                    'plaats': filteredAdres[0]
                }
            if len(filteredAdres) == 2:
                return {
                    'postcode': filteredAdres[0],
                    'plaats': filteredAdres[1]
                }
            if len(filteredAdres) == 3:
                return {
                    'postcode': filteredAdres[0] + filteredAdres[1],
                    'plaats': filteredAdres[2]
                }
            else:
                return {
                    'postcode': filteredAdres[0] + filteredAdres[1],
                    'plaats': filteredAdres[2] + " " + filteredAdres[3]
                }
          
    
        return None
    def parse_kamers(self,listing):
        kamers = listing.css('.search-result-kenmerken li:nth-child(2)::text').get()
        if kamers != None:
             if kamers.strip()[0].isdigit():
                return kamers
        print("is zero")
        return "0"
    def parse_prijs(self,listing):
        prijs = listing.css('.search-result-price::text').get().split(" ")
        for p in prijs:
            if p[0].isdigit():
                return p
        return "0"
    def parse_oppervlakte(self,listing):
        oppervlakte = listing.css('.search-result-kenmerken li:nth-child(1) span::text').get()
        if oppervlakte == None :
            return "0"
        else:
            splitOppervlakte = oppervlakte.split(" ")
            if splitOppervlakte[0][0].isdigit():
                return splitOppervlakte[0]
            

from sqlalchemy import create_engine, Column, Table, ForeignKey, MetaData
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Integer, String, Date, DateTime, Float, Boolean, Text)
from scrapy.utils.project import get_project_settings

Base = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(get_project_settings().get("CONNECTION_STRING"))


def create_table(engine):
    Base.metadata.create_all(engine)

class Listing(Base):
  __tablename__ = "listing"

  id = Column(Integer, primary_key = True)
  straat = Column('straat', String(200))
  postcode = Column('postcode', String(10), nullable=True)
  plaats = Column('plaats', String(100))
  prijs = Column('prijs', Integer, nullable=True)
  oppervlakte = Column('oppervlakte', Integer, nullable=True)
  kamers = Column('kamers', Integer, nullable=True)
  listing_id = Column('listing_id', String(200), unique=True)
  listing_url = Column('listing_url', String(200))
  site_id = Column('site_id', Integer)
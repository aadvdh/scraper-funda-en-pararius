# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item,Field
from scrapy.loader.processors import MapCompose, TakeFirst, Identity
import re

def strip_text(number):
    return re.sub("[^0-9]", "", number)
def add_domain(url, loader_context):
    return loader_context.get('domain') + url
def to_int(string):
    if hasNumbers(string):
        return int(string)
    else:
        return 0
def strip_whitespace(text):
    
    return text.strip()
def hasNumbers(string):
    return any(char.isdigit() for char in string)

class Listing(Item):
    straat = Field(input_processor=MapCompose(strip_whitespace), 
        output_processor=TakeFirst())
    postcode = Field(input_processor=MapCompose(),
    output_processor=TakeFirst())
    plaats = Field(input_processor=MapCompose(),
    output_processor=TakeFirst())
    prijs = Field(input_processor=MapCompose(strip_text, to_int),
                    output_processor=TakeFirst()
    )
    oppervlakte =  Field(input_processor=MapCompose(strip_whitespace, strip_text, to_int),
                    output_processor=TakeFirst()
    )
    kamers =  Field(input_processor=MapCompose(strip_whitespace, strip_text,to_int),
                    output_processor=TakeFirst())
    
    listing_id = Field(input_processor=MapCompose(), 
        output_processor=TakeFirst()
    )
    listing_url = Field(input_processor=MapCompose(add_domain), 
        output_processor=TakeFirst()
    )
    
    site_id = Field(input_processor=MapCompose(),
    output_processor=TakeFirst())